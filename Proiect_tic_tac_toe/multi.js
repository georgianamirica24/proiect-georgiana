let update = document.querySelector(".status-text");
let tiles = Array.from(document.querySelectorAll(".tile"));
let playAgain = document.querySelector(".play-again");

let player = true;
let moves = 1;
let score = {
    x: 0,
    o: 0,
    ties: 0,
};
const validSymbols = ['x', 'o'];
const players = {one: {name: '', symbol: '', first: false}, two: {name: '', symbol: '', first: false}}
update.textContent = 'Phase pick'
const handlePlayerOne = (e) => {
    if (!players.two.first) players.one.first = true;
    players.one.name = e.target.value;
}

const handlePlayerTwo = (e) => {
    if (!players.one.first) players.two.first = true;
    players.two.name = e.target.value;
}

const getPlayer = (first) => {
    if (first) {
        for (const property in players) {
            if (players[property].first) {
                return players[property].name;
            }
        }
    }
    for (const property in players) {
        if (!players[property].first) {
            return players[property].name
        }
    }
}

const getPlayerBySymbol = (symbol) => {
    for (const property in players) {
        if (players[property].symbol === symbol.toLowerCase()) {
            return players[property];
        }
    }
}

const begin = () => {
    if(!players.one.name || !players.two.name) {
        alert('I need to know your names first please !');
        return;
    }
    let firstPlayersName = null
    let firstPlayerIdentifier = null
    let secondPlayerIdentifier = null;
    for (const property in players) {
        if (players[property].first) {
            firstPlayerIdentifier = property;
            firstPlayersName = players[property].name;
        } else {
            secondPlayerIdentifier = property;
        }
    }
    while (true) {
        const symbol = prompt(`${firstPlayersName} gets to pick his symbol !`, 'x');
        if (validSymbols.includes(symbol.toLowerCase())) {
            players[firstPlayerIdentifier].symbol = symbol;
            players[secondPlayerIdentifier].symbol = validSymbols.find(sym => sym !== symbol.toLowerCase());
            break;
        }
    }
    document.querySelector('#pick').setAttribute('style', 'display: none;');
    document.querySelector('#playerx').textContent = players[firstPlayerIdentifier].name;
    document.querySelector('#playero').textContent = players[secondPlayerIdentifier].name;
    resetBoard();
}

document.querySelector('#player1').addEventListener('change', handlePlayerOne);
document.querySelector('#player2').addEventListener('change', handlePlayerTwo);
updateScore();

function resetBoard() {
    moves = 1;
    document.body.classList.add('x');

    update.textContent = getPlayer(true) + "'s turn";
    tiles.forEach((tile) => {
        tile.textContent = "";
        tile.classList.remove("tile-off");
        tile.classList.add("active");
        tile.addEventListener("click", tictactoe);
    });
    playAgain.classList.toggle("button-on");
}

function disableBoard() {
    document.body.classList.remove("x");
    document.body.classList.remove("o");
    tiles.forEach((tile) => {
        tile.removeEventListener("click", tictactoe);
        tile.classList.remove("active");
    });
}

function updateScore() {
    document.querySelector(".score-x").textContent = score.x;
    document.querySelector(".score-ties").textContent = score.ties;
    document.querySelector(".score-o").textContent = score.o;
}

function tictactoe(event) {
    let tile = event.target;
    let currentPlayer = document.body.className.toUpperCase();
    if (!tile.className.includes("tile")) return;
    if (tile.textContent) return;
    tile.textContent = currentPlayer;
    tile.removeEventListener("click", tictactoe);
    tile.classList.remove("active");
    let winner = findWinner();
    if (winner) {
        update.textContent = getPlayerBySymbol(currentPlayer).name.toUpperCase() + " wins!";
        if (currentPlayer === "X") {
            score.x += 1;
            player = false;
        } else {
            score.o += 1;
            player = true;
        }
        tiles
            .filter((tile, index) => winner.indexOf(index) < 0)
            .forEach((tileOff) => tileOff.classList.add("tile-off"));

        disableBoard();
        updateScore();
        playAgain.addEventListener("click", resetBoard);
        playAgain.classList.toggle("button-on");
    } else if (moves === 9) {
        // it's a tie
        update.textContent = "It's a tie!";
        score.ties += 1;
        disableBoard();
        updateScore();
        playAgain.addEventListener("click", resetBoard);
        playAgain.classList.toggle("button-on");
    } else {
        moves = moves + 1;
        player = !player;
        document.body.classList.toggle("x");
        document.body.classList.toggle("o");
        update.textContent = getPlayerBySymbol(document.body.className === 'x' ? 'o' : 'x').name + "'s turn";
    }
}

function findWinner() {
    let winningCombination = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    let winningSet = winningCombination.findIndex((combo) =>
        combo.every(
            (tileIndex) =>
                tiles[tileIndex].textContent === document.body.className.toUpperCase()
        )
    );
    return winningSet < 0 ? false : winningCombination[winningSet];
}
