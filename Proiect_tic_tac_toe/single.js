// @ts-nocheck
let square1;
let square2;
let square3;
let square4;
let square5;
let square6;
let square7;
let square8;
let square9;

let update = document.querySelector(".status-text");
let player = 'x'
let tiles = Array.from(document.querySelectorAll(".tile"));
let playAgain = document.querySelector(".play-again");
let XWins = 0;
let OWins = 0;

document.getElementById("statusText").innerText = "Phase pick";

let moves = 0;

let IDs = [];

const populateIds = () => {
    IDs = ["1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",]
}

const pick = (symbol) => {
    document.querySelector('#pick').setAttribute('style', 'display: none;');
    player = symbol;
    document.body.classList.remove('x');
    document.body.classList.remove('o');
    document.body.classList.add(player);
    resetBoard();
    if (player === 'o') {
        AI();
    }
}

function clicker(id) {
    if (moves === 0 || moves === 2 || moves % 2 === 0) {
        document.getElementById(id).innerHTML = "X";
    }
    else document.getElementById(id).innerHTML = "X";

    document.getElementById(id).onclick = null;

    moves++;

    if (moves === 9) {
        userX();
        document.getElementsById("statusText").innerText = "It's a tie!";
        unclick(true);
    }

    userX();
}

function state() {
    document.getElementById("statusText").innerText = `${player.toUpperCase()}'s turn`;
    square1 = document.getElementById("1").innerHTML;
    square2 = document.getElementById("2").innerHTML;
    square3 = document.getElementById("3").innerHTML;
    square4 = document.getElementById("4").innerHTML;
    square5 = document.getElementById("5").innerHTML;
    square6 = document.getElementById("6").innerHTML;
    square7 = document.getElementById("7").innerHTML;
    square8 = document.getElementById("8").innerHTML;
    square9 = document.getElementById("9").innerHTML;
}

function userX() {
    state();
    document.body.className = 'x'
    if (
        (square1 === "X" && square2 === "X" && square3 === "X") ||
        (square4 === "X" && square5 === "X" && square6 === "X") ||
        (square7 === "X" && square8 === "X" && square9 === "X") ||
        (square1 === "X" && square5 === "X" && square9 === "X") ||
        (square1 === "X" && square4 === "X" && square7 === "X") ||
        (square2 === "X" && square5 === "X" && square8 === "X") ||
        (square3 === "X" && square6 === "X" && square9 === "X") ||
        (square7 === "X" && square5 === "X" && square3 === "X")
    ) {
        document.getElementById("statusText").innerText = "X wins!";
        unclick(true);
        selectWinner();
        XWins++;
        document.querySelector(`.score-x`).innerText = `${XWins}`;
        playAgain.classList.toggle("button-on");
        return true;
    }

    return false;
}

function userO() {
    document.body.className = 'o'
    if (
        (square1 === "O" && square2 === "O" && square3 === "O") ||
        (square4 === "O" && square5 === "O" && square6 === "O") ||
        (square7 === "O" && square8 === "O" && square9 === "O") ||
        (square1 === "O" && square5 === "O" && square9 === "O") ||
        (square1 === "O" && square4 === "O" && square7 === "O") ||
        (square2 === "O" && square5 === "O" && square8 === "O") ||
        (square3 === "O" && square6 === "O" && square9 === "O") ||
        (square7 === "O" && square5 === "O" && square3 === "O")
    ) {
        document.getElementById("statusText").innerHTML = "O wins!";
        unclick(true);
        selectWinner();
        OWins++;
        document.querySelector(`.score-o`).innerText = `${OWins}`;
        playAgain.classList.toggle("button-on");
        return true;
    }
    return false;
}

function unclick(value) {
    for (var i = 1; i < 10; i++) {
        document.getElementById(i.toString()).setAttribute("disabled", value);
    }
}

const tie = () => {
    document.getElementById("statusText").innerHTML = "It's a tie!";
    unclick(true);
}

function clickerAI(id) {
    const target = document.querySelector(`td[id="${id}"]`);
    if (target.innerHTML.length) return;
    target.classList.remove('active');
    document.body.classList.add(player);
    document.getElementById(id).innerHTML = player.toUpperCase();

    var index = IDs.indexOf(id);

    IDs.splice(index, 1);

    moves++;

    if (moves < 8) {
        AI();
    }
    const boxes = tiles.map(tile => (tile.innerHTML)).filter(tile => tile.length > 0);
    if (userX() || userO()) return;

    if (moves >= 9) {
        tie();
        return;
    }

    moves++;
    if (boxes.length === 9) {
        tie();
    }
    document.body.className = player
}

function AI() {
    len = IDs.length - 1;
    rand = Math.random();
    random = Math.round(rand * len);

    target = IDs[random];
    document.getElementById(target).innerHTML = player === 'x' ? 'O' : 'X';
    document.getElementById(target).classList.remove('active');
    document.getElementById(target).onclick = null;

    index = IDs.indexOf(target);
    IDs.splice(index, 1);
    document.getElementById("statusText").innerText = `${player.toUpperCase()}'s turn`;
}

function checkIdSign(val1, val2, val3, sign) {
    if (
        getIdVal(val1) === sign &&
        getIdVal(val2) === sign &&
        getIdVal(val3) === sign
    ) {
        return true;
    }
    return false;
}

function selectWinner() {
    const winner = findWinner();
    if (winner) {
        tiles
            .filter((tile, index) => winner.indexOf(index) < 0)
            .forEach((tileOff) => tileOff.classList.add("tile-off"));
    }
}

playAgain.addEventListener("click", resetBoard);
playAgain.classList.toggle("button-on");

function resetBoard() {
    const replay = moves > 0;
    populateIds();
    moves = 0;
    document.getElementById("statusText").innerText = `X's turn!`;
    playAgain.style.display = "block";
    document.body.classList.add(player);
    unclick(false)
    tiles.forEach((tile) => {
        tile.textContent = "";
        tile.innerHTML = null;
        tile.classList.remove("tile-off");
        tile.classList.add("active");
        tile.onclick = clickerAI.bind(this, tile.getAttribute('id'))
    });
    if (replay) {
        if (player === 'o') {
            AI();
        }
    }
}

function findWinner() {
    let winningCombination = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    let winningSet = winningCombination.findIndex((combo) =>
        combo.every(
            (tileIndex) =>
                tiles[tileIndex].textContent === document.body.className.toUpperCase()
        )
    );
    return winningSet < 0 ? false : winningCombination[winningSet];
}
