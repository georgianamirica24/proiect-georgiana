// 6.1 Declare and asign variables

const nameOfTheSpaceShuttle= `Determination`;
const shuttleSpeed = 17500;
const distanceToMars = 225000000;
const distanceToMoon = 384400;
const milesPerKm = 0.621;

// 6.2 Calculate a space mission!

let milesToMars = distanceToMars*milesPerKm;
let hoursToMars = milesToMars/shuttleSpeed;
let daysToMars = hoursToMars/24;

// 6.3 Print out the result of your calculations

alert(`${nameOfTheSpaceShuttle} will take ${daysToMars} days to reach Mars.`);

// 6.4 Now calculate a trip to the Moon

let milesToMoon = distanceToMoon*milesPerKm;
let hoursToMoon = milesToMoon/shuttleSpeed;
let daysToMoon = hoursToMoon/24;

alert(`${nameOfTheSpaceShuttle} will take ${daysToMoon} days to reach Moon.`);