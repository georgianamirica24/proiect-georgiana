// The recipe card

let recipe = {
    'title': 'Mole',
    'servings': 2,
    'ingredients': ['cumin', 'cinnamon', 'cocoa']
};

console.log(recipe.title);
console.log('Servings: ' + recipe.servings);
console.log('Ingredients:');
for (let i = 0; i < recipe.ingredients.length; i++) {
    console.log(recipe.ingredients[i]);
}

// The reading list

let books = [
    {title: 'The Design of EveryDay Things',
     author: 'Don Norman',
     alreadyRead: false
    },
    {title: 'The Most Human Human',
    author: 'Brian Christian',
    alreadyRead: true
    }];
  
  for (let i = 0; i < books.length; i++) {
    let book = books[i];
    let bookInfo = book.title + '" by ' + book.author;
    if (book.alreadyRead) {
      console.log('You already read "' + bookInfo);
    } else {
      console.log('You still need to read "' + bookInfo);
    }
  }

// The movie database

var myFavMovie = {
    title: 'Puff the Magic Dragon',
    duration: 30,
    stars: ['Puff', 'Jackie', 'Living Sneezes']
};

function printMovie(movie) {
    console.log(movie.title + ' lasts for ' + movie.duration + ' minutes');
    let starsString = 'Stars: ';
    for (let i = 0; i < movie.stars.length; i++) {
        starsString += movie.stars[i];
        if (i != movie.stars.length -1) {
            starsString += ', ';
        }
    }
    console.log(starsString);
}
printMovie(myFavMovie);

// Credit card validation

function validateCreditCard(creditCardNum){
    if(creditCardNum.length !== 16){
    return false;
    }
  
    for(let i = 0; i < creditCardNum.length; i++){
      let currentNumber = creditCardNum[i];
      currentNumber = Number.parseInt(currentNumber);
      if(!Number.isInteger(currentNumber)){
        return false;
      }
    }

    let obj = {};
    for(let i = 0; i < creditCardNum.length; i++){
      obj[creditCardNum[i]] = true;
    }
    if(Object.keys(obj).length < 2){
      return false;
    }
  

    if(creditCardNum[creditCardNum.length - 1] % 2 !== 0){
      return false;
    }
    let sum = 0;
    for(let i = 0; i < creditCardNum.length; i++){
      sum += Number(creditCardNum[i]);
    }
    if(sum <= 16){
      return false;
    }
  
    return true;
}

  console.log(validateCreditCard('4243822015975580')); 
  console.log(validateCreditCard('6666666666661666')); 
  console.log(validateCreditCard('a9233b119c011d12'));
  console.log(validateCreditCard('4444444444444444')); 
  console.log(validateCreditCard('1211111171111112'));

