// 1. The Fortune Teller

function tellFortune(nrOfChildren, partnerName, geoLocation, jobTitle) {
    console.log(`You will be a ${jobTitle} in ${geoLocation}, and married to ${partnerName} with ${nrOfChildren} kids.`);
}

tellFortune(1, `Dani`, `Cluj`, `engineer`);
tellFortune(2, `Razvan`, `Iasi`, `architect`);
tellFortune(3, `Sebastian`, `Brasov`, `sales manager`);

// 2. The puppy age calculator

function calculateDogAge(puppyAge) {
    let dogYears= 7*puppyAge ;
    let humanYears = dogYears/7 ;
    
    console.log(`Your doggie is ${dogYears} years old in dog years!`);
    console.log(`Your doggie is ${humanYears} years old in human years!`);
} 

calculateDogAge(4);
calculateDogAge(5);
calculateDogAge(18);

// 3. The lifetime supply calculator

function calculateSupply(age, amountPerDay) {
    const maximumAge = 90;
    let result = (maximumAge-age)*365*amountPerDay+(maximumAge-age)/4*amountPerDay;
    console.log(`You will need ${Math.round(result)} to last you until the ripe old age of ${maximumAge}.`);
}

calculateSupply(20, 3);
calculateSupply(21, 2);
calculateSupply(23, 4);

// 4. The Geometrizer

function calcCircumference(radius) {
    const pi = 3.141592;
    console.log(`The circumference is ${2*pi*radius}`);
}

calcCircumference(45);

function calcArea(radius) {
    const pi = 3.141592;
    console.log(`The area is ${pi*Math.pow(radius,2)}`);
}

calcArea(45);

// 5. The Temperature Converter

function celsiusToFahrenheit(celsiusTemp) {
    console.log(`${celsiusTemp}°C is ${celsiusTemp*1.8+32}°F.`);
}

celsiusToFahrenheit(24);

function fahrehnheitToCelsius(fahrehnheitTemp) {
    console.log(`${fahrehnheitTemp}°F is ${(fahrehnheitTemp-32)/1.8}°C.`)
}

fahrehnheitToCelsius(95);

// 6. The calculator

function squareNumber(someNr) {
    let square = Math.pow(someNr, 2);
    console.log(`The result of squaring the number ${someNr} is ${square}.`);
    return square;
}

squareNumber(5);

function halfNumber(someNr) {
    let half = someNr/2;
    console.log(`Half of ${someNr} is ${half}`);
    return half;
}

halfNumber(18);

function percentOf (firstNr, secondNr) {
    let procent = (secondNr*100)/firstNr;
    console.log(`${secondNr} is ${procent}% of ${firstNr}`);
    return procent;
}
percentOf (96, 24);

function areaOfCircle(radius) {
    const pi = 3.141592;
    let area = pi*Math.pow(radius,2)
    console.log(`The area of circle with radius ${radius} is ${area.toFixed(2)}.`);
    return area;
}

areaOfCircle(45);

function function5(someNr) {
    let half5 = halfNumber(someNr);
    let square5 = squareNumber(half5);
    let area5 = areaOfCircle(square5);
    let procent5 = percentOf(square5, area5);
}
function5(4);

// 7. DrEvil

function DrEvil(amount) {
    if(amount==1000000)
    console.log(`${amount}$ (pinky)`);
    else console.log(`${amount}$`);

}

DrEvil(1000000);

// 8. MixUp

function mixUp(string1, string2) {
    string2_result = string1.slice(0, 2);
    string1_result = string2.slice(0, 2);
    console.log(`${string1_result}${string1.slice(2)} ${string2_result}${string2.slice(2)}`);
}

mixUp(`mix`, `pod`);
mixUp(`dog`, `dinner`);

// 9. FixStart

function fixStart(s) {
  var c = s.charAt(0);
  return c + s.slice(1).replace(c, '*');
}

console.log(fixStart('babble'))

// 10. Verbing

function verbing(word) {
    if (word.length < 3) return word;
    if (word.slice(-3) == 'ing') {
      return word + 'ly';
    } else {
      return word + 'ing';
    }
}

console.log(verbing(`swim`))
console.log(verbing(`swimming`))
console.log(verbing(`go`))

//11. Not bad

function notBad(sentence) {
    var notIndex = sentence.indexOf('not');
    var badIndex = sentence.indexOf('bad');
    if (notIndex == -1 || badIndex == -1 || badIndex < notIndex) return sentence;
    return sentence.slice(0, notIndex) + 'good' + sentence.slice(badIndex + 3);
}

console.log(notBad(`This dinner is not that bad!`))
console.log(notBad(`This movie is not so bad!`))
console.log(notBad(`This dinner is bad!`))
