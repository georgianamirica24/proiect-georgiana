class ValidatorError extends Error {
    constructor(message) {
        super(message);
        this.name = "ValidationError";
    }

    
}

let json = `{"name": "John" , "age" : 30 }`;
let user = JSON.parse(json);

let readObject = function (Object) {
  let error = 0;
  if (!Object.name) {
    throw new Error("There is no name registered.");
    error++;
  }
  if (!Object.age) {
    throw new Error("There is no age registered.");
    error++;
  }
  if (error == 0) {
    alert(`${Object.name} is ${Object.age}.`);
    return Object;
  }
};
try {
  readObject(user);
} catch (err) {
  alert(err.message);
}
